package com.shivam.diagnal.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.res.ResourcesCompat
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.shivam.diagnal.R
import com.shivam.diagnal.databinding.ItemContentListGridBinding
import com.shivam.diagnal.model.Content

class ContentListRecyclerAdapter(var context: Context, var contentList: ArrayList<Content>) :
        RecyclerView.Adapter<ContentListRecyclerAdapter.ContentViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ContentViewHolder {
        val binding: ItemContentListGridBinding =
                DataBindingUtil.inflate(LayoutInflater.from(parent.context),
                        R.layout.item_content_list_grid, parent, false)

        return ContentViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return this.contentList.size
    }

    override fun onBindViewHolder(holder: ContentViewHolder, position: Int) {
        holder.bind(contentList[position] as Content)
    }

    fun updateList(contentList: ArrayList<Content>) {
        this.contentList = contentList
        notifyDataSetChanged()
    }

    inner class ContentViewHolder(var itemContentListGridBinding: ItemContentListGridBinding) :
            RecyclerView.ViewHolder(itemContentListGridBinding.root) {
        fun bind(content: Content) {
            itemContentListGridBinding.contentImageView.setImageDrawable(
                    ResourcesCompat.getDrawable(context.resources,
                            getMoviePosterDrawable(content.posterImage), context.theme))

            itemContentListGridBinding.titleTextView.text = content.name
        }
    }

    private fun getMoviePosterDrawable(posterImage: String): Int {
        return when {
            posterImage.contains("1") -> R.drawable.poster1
            posterImage.contains("2") -> R.drawable.poster2
            posterImage.contains("3") -> R.drawable.poster3
            posterImage.contains("4") -> R.drawable.poster4
            posterImage.contains("5") -> R.drawable.poster5
            posterImage.contains("6") -> R.drawable.poster6
            posterImage.contains("7") -> R.drawable.poster7
            posterImage.contains("8") -> R.drawable.poster8
            posterImage.contains("9") -> R.drawable.poster9
            else -> R.drawable.placeholder_for_missing_posters
        }
    }
}