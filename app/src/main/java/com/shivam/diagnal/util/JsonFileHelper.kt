package com.shivam.diagnal.util

import android.content.Context
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.shivam.diagnal.model.Data

fun Context.readJsonAsset(fileName: String): String {
    val inputStream = assets.open(fileName)
    val size = inputStream.available()
    val buffer = ByteArray(size)
    inputStream.read(buffer)
    inputStream.close()
    return String(buffer, Charsets.UTF_8)
}

fun Context.getPageObjectFromJsonString(jsonDataString: String): Data {

    var gson = Gson()
    val dataType = object : TypeToken<Data>() {}.type
    return gson.fromJson(jsonDataString, dataType)
}
