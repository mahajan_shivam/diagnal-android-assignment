package com.shivam.diagnal.model

import com.google.gson.annotations.SerializedName

data class Page (
        @SerializedName("title") val title: String,
        @SerializedName("total-content-items") val totalCount: String,
        @SerializedName("page-num") val pageNo: String,
        @SerializedName("page-size") val pageSize: String,
        @SerializedName("content-items") val contentItems: ContentItems)