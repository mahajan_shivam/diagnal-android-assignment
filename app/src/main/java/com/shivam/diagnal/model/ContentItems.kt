package com.shivam.diagnal.model

import com.google.gson.annotations.SerializedName

data class ContentItems(@SerializedName("content") var contentList: ArrayList<Content>)