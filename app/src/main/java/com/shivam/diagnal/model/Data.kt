package com.shivam.diagnal.model

import com.google.gson.annotations.SerializedName

data class Data(@SerializedName("page") var page: Page)