package com.shivam.diagnal.viewmodel

import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.shivam.diagnal.model.Data
import com.shivam.diagnal.repository.ContentListRepository

class ContentListViewModel : ViewModel() {

    private var pageLiveData: MutableLiveData<Data>? = null
    private var contentListRepo = ContentListRepository()

    fun getPageLiveData(context: Context, fileName: String): MutableLiveData<Data>? {
        if (pageLiveData == null) {
            pageLiveData = contentListRepo.getPageLiveData(context, fileName)
        }
        return pageLiveData
    }

    fun getNextPageData(context: Context, fileName: String) {
        contentListRepo.getNextPageData(context, fileName)
    }
}