package com.shivam.diagnal.repository

import android.content.Context
import androidx.lifecycle.MutableLiveData
import com.google.gson.Gson
import com.shivam.diagnal.model.Data
import com.shivam.diagnal.util.getPageObjectFromJsonString
import com.shivam.diagnal.util.readJsonAsset

class ContentListRepository {

    private var pageLiveData: MutableLiveData<Data>? = null
    private val gson = Gson()

    fun getPageLiveData(context: Context, fileName: String): MutableLiveData<Data>? {
        if (pageLiveData == null) {
            pageLiveData = MutableLiveData()
            val pageDataJson = context.readJsonAsset(fileName)
            val pageDataObj = context.getPageObjectFromJsonString(pageDataJson)
            pageLiveData?.postValue(pageDataObj)
        }
        return pageLiveData
    }

    fun getNextPageData(context: Context, fileName: String) {
        val pageDataJson = context.readJsonAsset(fileName)
        val pageDataObj = context.getPageObjectFromJsonString(pageDataJson)
        pageLiveData?.postValue(pageDataObj)
    }
}