package com.shivam.diagnal.activity

import android.app.SearchManager
import android.content.Context
import android.content.res.Configuration
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.View
import android.widget.SearchView
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.shivam.diagnal.R
import com.shivam.diagnal.adapter.ContentListRecyclerAdapter
import com.shivam.diagnal.databinding.ActivityContentListBinding
import com.shivam.diagnal.model.Content
import com.shivam.diagnal.model.Data
import com.shivam.diagnal.viewmodel.ContentListViewModel

class ContentListActivity : AppCompatActivity() {

    private val PORTRAIT_SPAN_COUNT = 3
    private val LANDSCAPE_SPAN_COUNT = 7
    private lateinit var contentListViewModel: ContentListViewModel
    private lateinit var contentListActivityBinding: ActivityContentListBinding
    private lateinit var adapter: ContentListRecyclerAdapter
    private lateinit var layoutManager: GridLayoutManager
    private var contentArrayList: ArrayList<Content> = ArrayList()
    private var pageNum = 1
    private var loading = true
    private var pastVisibleItems = 0
    private var visibleItemCount: Int = 0
    private var totalItemCount: Int = 0
    private val MAX_PAGE_SIZE = 20
    private var query: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        contentListActivityBinding =
                DataBindingUtil.setContentView(this, R.layout.activity_content_list)
        contentListViewModel = ViewModelProvider(this).get(ContentListViewModel::class.java)

        supportActionBar?.setHomeButtonEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        initRecyclerView()
    }

    private var pageDataObserver: Observer<Data> = Observer<Data> {
        val contentList = it.page.contentItems.contentList
        if (contentList.isEmpty()) {
            loading = false
            return@Observer
        }
        contentArrayList.addAll(contentList)
        adapter.updateList(contentArrayList)
        loading = it.page.pageSize.toInt() % MAX_PAGE_SIZE == 0
    }

    private fun initRecyclerView() {
        contentListViewModel.getPageLiveData(this,
                getString(R.string.fileNameStr) + pageNum + getString(R.string.fileExtStr))
                ?.observe(this, pageDataObserver)

        layoutManager = GridLayoutManager(this, PORTRAIT_SPAN_COUNT)
        contentListActivityBinding.recyclerView.layoutManager = layoutManager
        layoutManager.spanCount = PORTRAIT_SPAN_COUNT
        adapter = ContentListRecyclerAdapter(contentListActivityBinding.recyclerView.context,
                contentArrayList)
        contentListActivityBinding.recyclerView.adapter = adapter

        contentListActivityBinding.recyclerView.addOnScrollListener(recyclerViewScrollListener)
    }

    private val recyclerViewScrollListener: RecyclerView.OnScrollListener =
            object : RecyclerView.OnScrollListener() {
                override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                    if (dy > 0) { //check for scroll down
                        visibleItemCount = layoutManager.childCount
                        totalItemCount = layoutManager.itemCount
                        pastVisibleItems = layoutManager.findFirstVisibleItemPosition()
                        if (loading) {
                            if (visibleItemCount + pastVisibleItems >= totalItemCount - 10) {
                                loading = false
                                Log.d("...", "Last Item Wow !")
                                pageNum++
                                contentListViewModel.getNextPageData(this@ContentListActivity,
                                        getString(R.string.fileNameStr) + pageNum + getString(
                                                R.string.fileExtStr))
                            }
                        }
                    }
                }
            }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            layoutManager.spanCount = LANDSCAPE_SPAN_COUNT
        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
            layoutManager.spanCount = PORTRAIT_SPAN_COUNT
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.options_menu, menu)
        val searchManager = getSystemService(Context.SEARCH_SERVICE) as SearchManager
        val searchView = menu.findItem(R.id.search).actionView as SearchView
        searchView.setSearchableInfo(searchManager.getSearchableInfo(componentName))

        searchView.setOnCloseListener {
            handleSearchQuery()
            true
        }

        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(p0: String?): Boolean {
                if (p0 != null) {
                    query = p0
                }
                handleSearchQuery()
                return true
            }

            override fun onQueryTextChange(p0: String?): Boolean {
                if (p0 != null) {
                    query = p0
                }
                handleSearchQuery()
                return true
            }
        })


        return true
    }

    private fun handleSearchQuery() {

        if (query != null) {
            if (query.length >= 3) {
                val filteredList = contentArrayList.filter { contentObj ->
                    contentObj.name.contains(query, true)
                }
                if (filteredList.isNotEmpty()) {
                    adapter.updateList(filteredList as ArrayList<Content>)
                    contentListActivityBinding.recyclerView.clearOnScrollListeners()
                    contentListActivityBinding.recyclerView.visibility = View.VISIBLE
                    contentListActivityBinding.noDataTextView.visibility = View.GONE
                } else {
                    contentListActivityBinding.recyclerView.visibility = View.GONE
                    contentListActivityBinding.noDataTextView.visibility = View.VISIBLE
                }
            } else if (query.isEmpty()) {
                adapter.updateList(contentArrayList)
                contentListActivityBinding.recyclerView.visibility = View.VISIBLE
                contentListActivityBinding.noDataTextView.visibility = View.GONE
                contentListActivityBinding.recyclerView.addOnScrollListener(
                        recyclerViewScrollListener)
            }
        }
    }
}