package com.shivam.diagnal.application

import android.app.Application
import com.shivam.diagnal.di.AppUtilDependency
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class DiagnalApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        initKoin()
    }

    private fun initKoin() {
        startKoin {
            androidContext(this@DiagnalApplication)
            modules(provideDependency())
        }
    }

    open fun provideDependency() = appComponent

    val appComponent = listOf(AppUtilDependency)
}